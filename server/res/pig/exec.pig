data = LOAD 'ip' USING PigStorage(',') AS (user_id : chararray, word : chararray);
users = FILTER data BY user_id != '$user_id' ; 
usage = FILTER users BY word == '$word' ;
word = GROUP usage ALL;
cnt = FOREACH word GENERATE COUNT(usage);
STORE cnt INTO 'out' USING PigStorage(',');
