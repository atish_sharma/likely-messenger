// Requiring node modules and instantiating essential objects
require('node-env-file')('env');
var express = require('express');
var app = express();
var mongo = require('mongodb');
var monk = require('monk');
var http = require('http')
var server = http.createServer(app);
var io = require('socket.io')(server);
var _ = require('underscore');

// Configuring app to use static files
app.use('/app', express.static(__dirname + '/../app'));

// Setting variables
var TAG = 'server';
var PORT = process.env.PORT;
app.locals.dir = __dirname;

var DB = monk(process.env.DB_HOST + ':' + process.env.DB_PORT + '/' + process.env.DB_NAME, {
  username : process.env.DB_USERNAME,
  password : process.env.DB_PASSWORD
});

var LogModule = require('./src/Module/LogModule.js').LogModule;
var SocketModule = require('./src/Module/SocketModule.js').SocketModule;
var DatabaseModule = require('./src/Module/DatabaseModule.js').DatabaseModule;
var SocketController = require('./src/Controller/SocketController/SocketController.js').SocketController;
require('./src/Controller/RouteController/RouteController.js').RouteController(app);

DatabaseModule.setDatabase(DB);

// Listening on a port
server.listen(PORT, function(){
  LogModule.log(TAG, 'Listening on port ' + PORT);
});

// Listening for a connection
io.on(SocketModule.SOCKET_EVENT.CONNECT, function(socket) {
  var socketController = new SocketController(io);
  socketController.on(socket);
});