var LogModule = require('../../Module/LogModule.js').LogModule;
var SocketModule = require('../../Module/SocketModule.js').SocketModule;
var GoogleLoginController = require('./GoogleLoginController.js').GoogleLoginController;
var SessionLoginController = require('./SessionLoginController.js').SessionLoginController;
var LogoutController = require('./LogoutController.js').LogoutController;
var SessionClearanceController = require('./SessionClearanceController.js').SessionClearanceController;
var UserSearchController = require('./UserSearchController.js').UserSearchController;
var MessageController = require('./MessageController.js').MessageController;
var NotificationController = require('./NotificationController.js').NotificationController;
var ChatSeenController = require('./ChatSeenController.js').ChatSeenController;
var ChatController = require('./ChatController.js').ChatController;

var TAG = 'SocketController';

var SocketController = function(io) {

	LogModule.logConstructor(TAG);

	var self = this;
	self.io = io;
	self.socket = null;

	self.on = function(socket) {
		LogModule.log(TAG, 'on');
		self.socket = socket;
		LogModule.log(TAG, 'Connected to socket ' + self.socket.id);
		self.setListeners();
	};

	self.setListeners = function() {
		LogModule.log(TAG, 'setListeners');
		self.socket.on(SocketModule.SOCKET_EVENT.DISCONNECT, self.off);
		self.socket.on(SocketModule.SOCKET_EVENT.GOOGLE_LOGIN, function(data) {
			var googleLoginController = new GoogleLoginController(self.socket);
			googleLoginController.on(data);
		});
		self.socket.on(SocketModule.SOCKET_EVENT.SESSION_LOGIN, function(data) {
			var sessionLoginController = new SessionLoginController(self.socket);
			sessionLoginController.on(data);
		});
		self.socket.on(SocketModule.SOCKET_EVENT.LOGOUT, function(data) {
			var logoutController = new LogoutController(self.socket);
			logoutController.on(data);
		});
		self.socket.on(SocketModule.SOCKET_EVENT.SESSION_CLEARANCE, function(data) {
			var sessionClearanceController = new SessionClearanceController(self.io, self.socket);
			sessionClearanceController.on(data);
		});
		self.socket.on(SocketModule.SOCKET_EVENT.USER_SEARCH, function(data) {
			var userSearchController = new UserSearchController(self.socket);
			userSearchController.on(data);
		});
		self.socket.on(SocketModule.SOCKET_EVENT.MESSAGE, function(data) {
			var messageController = new MessageController(self.io, self.socket);
			messageController.on(data);
		});
		self.socket.on(SocketModule.SOCKET_EVENT.NOTIFICATION, function(data) {
			var notificationController = new NotificationController(self.io, self.socket);
			notificationController.on(data);
		});
		self.socket.on(SocketModule.SOCKET_EVENT.CHAT_SEEN, function(data) {
			var chatSeenController = new ChatSeenController(self.socket);
			chatSeenController.on(data);
		});

		self.socket.on(SocketModule.SOCKET_EVENT.CHAT, function(data) {
			var chatController = new ChatController(self.socket);
			chatController.on(data);
		});
	};

	self.off = function() {
		LogModule.log(TAG, 'off');
		LogModule.log(TAG, 'Disconnected from socket ' + self.socket.id);
	};

};

exports.SocketController = SocketController;