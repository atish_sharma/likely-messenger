var LogModule = require('../../Module/LogModule.js').LogModule;
var SocketModule  = require('../../Module/SocketModule.js').SocketModule;
var ApiModule = require('../../Module/ApiModule.js').ApiModule;
var ResponseGeneratorModule  = require('../../Module/ResponseGeneratorModule.js').ResponseGeneratorModule;
var UtilityModule  = require('../../Module/UtilityModule.js').UtilityModule;
var UserModel = require('../../Model/UserModel.js').UserModel;

var TAG = 'SessionClearanceController';

var SessionClearanceController = function(io, socket) {

	LogModule.logConstructor(TAG);

	var self = this;
	self.io = io;
	self.socket = socket;
	self.user = new UserModel();
	self.userId = null;

	self.on = function(data) {
		LogModule.log(TAG, 'on');
		LogModule.log(TAG, 'Session Clearance Data', data);
		var sessionId = UtilityModule.sanitize(data[ApiModule.KEY.SESSION_ID]);
		self.user.sessionLogin(sessionId, self.login);
	};

	self.login = function() {
		LogModule.log(TAG, 'login');
		if(self.user.isLoggedIn()) {
			self.userId = self.user.id;
			self.user.clearSessions(self.clearSessions);
		} else {
			self.emit(ResponseGeneratorModule.generateInvalidSessionResponse());
		}
	};

	self.clearSessions = function(success) {
		LogModule.log(TAG, 'clearSessions');
		if(success) {
			var clients = self.io.sockets.adapter.rooms[self.userId].sockets;
			for (var clientId in clients) {
				var socket = self.io.sockets.connected[clientId];
				try {
					socket.leave(self.userId);
				} catch(err) {
					LogModule.log(TAG, err);
				}
			}
			self.emit(ResponseGeneratorModule.generateSessionClearanceSuccessResponse());
		} else {
			self.emit(ResponseGeneratorModule.generateSessionClearanceFailedResponse());
		}
	};

	self.emit = function(data) {
		LogModule.log(TAG, 'emit');
		LogModule.log(TAG, 'Session Clearance Response', data);
		self.socket.emit(SocketModule.SOCKET_EVENT.SESSION_CLEARANCE, data);
	};

};

exports.SessionClearanceController = SessionClearanceController;