var LogModule = require('../../Module/LogModule.js').LogModule;
var SocketModule  = require('../../Module/SocketModule.js').SocketModule;
var ApiModule = require('../../Module/ApiModule.js').ApiModule;
var ResponseGeneratorModule  = require('../../Module/ResponseGeneratorModule.js').ResponseGeneratorModule;
var UtilityModule  = require('../../Module/UtilityModule.js').UtilityModule;
var UserModel = require('../../Model/UserModel.js').UserModel;

var TAG = 'SessionLoginController';

var SessionLoginController = function(socket) {

	LogModule.logConstructor(TAG);

	var self = this;
	self.socket = socket;
	self.user = new UserModel();

	self.on = function(data) {
		LogModule.log(TAG, 'on');
		LogModule.log(TAG, 'Session Login Data', data);
		var sessionId = UtilityModule.sanitize(data[ApiModule.KEY.SESSION_ID]);
		self.user.sessionLogin(sessionId, self.login);
	};

	self.login = function() {
		LogModule.log(TAG, 'login');
		if(self.user.isLoggedIn()) {
			self.user.getInfo(self.userInfo);
		} else {
			self.emit(ResponseGeneratorModule.generateInvalidSessionResponse());
		}
	};

	self.userInfo = function(success, googleId, name, email, picture, sessionId, userId, signupTimestamp, sessionTimestamp, activeSessions) {
		LogModule.log(TAG, 'userInfo');
		if(success) {
			self.socket.join(self.user.id);
			self.emit(ResponseGeneratorModule.generateLoginSuccessResponse(googleId, name, email, picture, sessionId, userId, signupTimestamp, sessionTimestamp, activeSessions));
		} else {
			self.emit(ResponseGeneratorModule.generateLoginFailedResponse());
		}
	};

	self.emit = function(data) {
		LogModule.log(TAG, 'emit');
		LogModule.log(TAG, 'Session Login Response', data);
		self.socket.emit(SocketModule.SOCKET_EVENT.SESSION_LOGIN, data);
	};

};

exports.SessionLoginController = SessionLoginController;