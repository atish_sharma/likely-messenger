var LogModule = require('../../Module/LogModule.js').LogModule;
var SocketModule  = require('../../Module/SocketModule.js').SocketModule;
var ApiModule = require('../../Module/ApiModule.js').ApiModule;
var DatabaseModule = require('../../Module/DatabaseModule.js').DatabaseModule;
var ResponseGeneratorModule  = require('../../Module/ResponseGeneratorModule.js').ResponseGeneratorModule;
var UtilityModule  = require('../../Module/UtilityModule.js').UtilityModule;
var UserModel = require('../../Model/UserModel.js').UserModel;

var TAG = 'UserSearchController';

var UserSearchController = function(socket) {

	LogModule.logConstructor(TAG);

	var self = this;
	self.socket = socket;
	self.user = new UserModel();
	self.email = null;
	self.limit = null;

	self.on = function(data) {
		LogModule.log(TAG, 'on');
		LogModule.log(TAG, 'User Search Data', data);
		var sessionId = UtilityModule.sanitize(data[ApiModule.KEY.SESSION_ID]);
		self.email = UtilityModule.sanitize(data[ApiModule.KEY.EMAIL]);
		self.limit = UtilityModule.sanitize(data[ApiModule.KEY.LIMIT]);
		self.user.sessionLogin(sessionId, self.login);
	};

	self.login = function() {
		LogModule.log(TAG, 'login');
		if(self.user.isLoggedIn()) {
			self.user.userSearch(self.email, self.limit, self.userSearch);
		} else {
			self.emit(ResponseGeneratorModule.generateInvalidSessionResponse());
		}
	};

	self.userSearch = function(success, users) {
		LogModule.log(TAG, 'userSearch');
		if(success) {
			var userList = [];
			for(var i = 0; i < users.length ; i++) {
				var user = {};
				user[ApiModule.KEY.NAME] = users[i][DatabaseModule.COLUMN.NAME];
				user[ApiModule.KEY.USER_ID] = users[i][DatabaseModule.COLUMN.ID];
				user[ApiModule.KEY.EMAIL] = users[i][DatabaseModule.COLUMN.EMAIL];
				user[ApiModule.KEY.PROFILE_PIC_URL] = users[i][DatabaseModule.COLUMN.PICTURE];
				user[ApiModule.KEY.GOOGLE_ID] = users[i][DatabaseModule.COLUMN.GOOGLE_ID];
				userList.push(user);
			}
			self.emit(ResponseGeneratorModule.generateUserSearchSuccessResponse(userList));
		} else {
			self.emit(ResponseGeneratorModule.generateUserSearchFailedResponse());
		}
	};

	self.emit = function(data) {
		LogModule.log(TAG, 'emit');
		LogModule.log(TAG, 'User Search Response', data);
		self.socket.emit(SocketModule.SOCKET_EVENT.USER_SEARCH, data);
	};

};

exports.UserSearchController = UserSearchController;