var LogModule = require('../../Module/LogModule.js').LogModule;
var SocketModule  = require('../../Module/SocketModule.js').SocketModule;
var ApiModule = require('../../Module/ApiModule.js').ApiModule;
var ResponseGeneratorModule  = require('../../Module/ResponseGeneratorModule.js').ResponseGeneratorModule;
var UtilityModule  = require('../../Module/UtilityModule.js').UtilityModule;
var DatabaseModule  = require('../../Module/DatabaseModule.js').DatabaseModule;
var UserModel = require('../../Model/UserModel.js').UserModel;
var MessageModel = require('../../Model/MessageModel.js').MessageModel;

var TAG = 'MessageController';

var MessageController = function(io, socket) {

	LogModule.logConstructor(TAG);

	var self = this;
	self.io = io;
	self.socket = socket;
	self.user = new UserModel();
	self.message = new MessageModel();

	self.on = function(data) {
		LogModule.log(TAG, 'on');
		LogModule.log(TAG, 'Message Data', data);
		var sessionId = UtilityModule.sanitize(data[ApiModule.KEY.SESSION_ID]);
		self.message.message = UtilityModule.sanitize(data[ApiModule.KEY.MESSAGE]);
		self.message.receiverId = UtilityModule.sanitize(data[ApiModule.KEY.RECEIVER_ID]);
		self.user.sessionLogin(sessionId, self.login);
	};

	self.login = function() {
		LogModule.log(TAG, 'login');
		if(self.user.isLoggedIn()) {
			self.user.getUserInfo(self.message.receiverId, function(success, name, email, googleId, userId, picture, sessions) {
				if(success) {
					self.message.senderId = self.user.id;
					self.message.receiverId = userId;
					self.message.save(self.sendMessage);
				} else {
					self.emit(ResponseGeneratorModule.generateMessageFailedResponse());
				}
			});
		} else {
			self.emit(ResponseGeneratorModule.generateInvalidSessionResponse());
		}
	};

	self.sendMessage  = function(success) {
		LogModule.log(TAG, 'sendMessage');
		if(success) {
			self.emitMessage();
		} else {
			self.emit(ResponseGeneratorModule.generateMessageFailedResponse());
		}
	};

	self.emit = function(data) {
		LogModule.log(TAG, 'emit');
		LogModule.log(TAG, 'Message Status Response to Sender', data);
		self.socket.emit(SocketModule.SOCKET_EVENT.MESSAGE_STATUS, data);
	};

	self.emitMessage = function() {
		LogModule.log(TAG, 'emitMessage');
		var msg = {};
		msg[ApiModule.KEY.MESSAGE] = self.message.message;
		msg[ApiModule.KEY.MESSAGE_ID] = self.message.id;
		msg[ApiModule.KEY.SENDER_ID] = self.message.senderId;
		msg[ApiModule.KEY.RECEIVER_ID] = self.message.receiverId;
		msg[ApiModule.KEY.MESSAGE_TIMESTAMP] = self.message.timestamp;
		msg[ApiModule.KEY.NOTIFIED] = self.message.notified;
		self.user.getInfo(function(success, googleId, name, email, picture, sessionId, userId, signupTimestamp, sessionTimestamp, activeSessions) {
			if(success) {
				msg[ApiModule.KEY.NAME] = name;
				msg[ApiModule.KEY.GOOGLE_ID] = googleId;
				msg[ApiModule.KEY.PROFILE_PIC_URL] = picture;
				msg[ApiModule.KEY.EMAIL] = email;
				LogModule.log(TAG, 'Message Response to Receiver & Sender', msg);
				self.io.to(self.message.receiverId).emit(SocketModule.SOCKET_EVENT.MESSAGE, msg);
				self.emit(ResponseGeneratorModule.generateMessageSuccessResponse());
				self.io.to(self.message.senderId).emit(SocketModule.SOCKET_EVENT.MESSAGE, msg);
				self.message.query(self.message.message, function(success, tags, users) {
					if(success) {
						var suggestions = {};
						suggestions[ApiModule.KEY.USERS] = [];
						for(var i=0;i<users.length;i++) {
							var user = {};
							user[ApiModule.KEY.NAME] = users[i][DatabaseModule.COLUMN.NAME];
							user[ApiModule.KEY.USER_ID] = users[i][DatabaseModule.COLUMN.ID];
							user[ApiModule.KEY.EMAIL] = users[i][DatabaseModule.COLUMN.EMAIL];
							user[ApiModule.KEY.PROFILE_PIC_URL] = users[i][DatabaseModule.COLUMN.PICTURE];
							user[ApiModule.KEY.GOOGLE_ID] = users[i][DatabaseModule.COLUMN.GOOGLE_ID];
							user[ApiModule.KEY.SCORE] = users[i]['score'];
							suggestions[ApiModule.KEY.USERS].push(user);
						}
						suggestions[ApiModule.KEY.TAGS] = tags;
						LogModule.log(TAG, 'Suggestion Response to Sender', suggestions);
						self.io.to(self.message.senderId).emit(SocketModule.SOCKET_EVENT.SUGGESTIONS, suggestions);
					}
				});
			} else {
				self.emit(ResponseGeneratorModule.generateMessageFailedResponse());
			}
		});
	};

};

exports.MessageController = MessageController;