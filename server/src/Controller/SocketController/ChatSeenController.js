var LogModule = require('../../Module/LogModule.js').LogModule;
var SocketModule  = require('../../Module/SocketModule.js').SocketModule;
var ApiModule = require('../../Module/ApiModule.js').ApiModule;
var DatabaseModule = require('../../Module/DatabaseModule.js').DatabaseModule;
var ResponseGeneratorModule  = require('../../Module/ResponseGeneratorModule.js').ResponseGeneratorModule;
var UtilityModule  = require('../../Module/UtilityModule.js').UtilityModule;
var UserModel = require('../../Model/UserModel.js').UserModel;
var MessageModel = require('../../Model/MessageModel.js').MessageModel;

var TAG = 'ChatSeenController';

var ChatSeenController = function(socket) {

	LogModule.logConstructor(TAG);

	var self = this;
	self.socket = socket;
	self.user = new UserModel();
	self.message = new MessageModel();

	self.on = function(data) {
		LogModule.log(TAG, 'on');
		LogModule.log(TAG, 'Chat Seen Data', data);
		var sessionId = UtilityModule.sanitize(data[ApiModule.KEY.SESSION_ID]);
		self.message.receiverId = UtilityModule.sanitize(data[ApiModule.KEY.RECEIVER_ID]);
		self.user.sessionLogin(sessionId, self.login);
	};

	self.login = function() {
		LogModule.log(TAG, 'login');
		if(self.user.isLoggedIn()) {
			self.user.getUserInfo(self.message.receiverId, function(success, name, email, googleId, userId, picture, sessions) {
				if(success) {
					self.message.senderId = self.user.id;
					self.message.receiverId = userId;
					self.message.setMessagesSeen(self.chatSeen);
				} else {
					self.emit(ResponseGeneratorModule.generateChatSeenFailedResponse());
				}
			});
		} else {
			self.emit(ResponseGeneratorModule.generateInvalidSessionResponse());
		}
	};

	self.chatSeen = function(success) {
		LogModule.log(TAG, 'chatSeen');
		if(success) {
			self.emit(ResponseGeneratorModule.generateChatSeenSuccessResponse());
		} else {
			self.emit(ResponseGeneratorModule.generateChatSeenFailedResponse());
		}
	};

	self.emit = function(data) {
		LogModule.log(TAG, 'emit');
		LogModule.log(TAG, 'Chat Seen Response', data);
		self.socket.emit(SocketModule.SOCKET_EVENT.CHAT_SEEN, data);
	};

};

exports.ChatSeenController = ChatSeenController;