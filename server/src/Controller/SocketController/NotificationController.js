var LogModule = require('../../Module/LogModule.js').LogModule;
var SocketModule  = require('../../Module/SocketModule.js').SocketModule;
var ApiModule = require('../../Module/ApiModule.js').ApiModule;
var DatabaseModule = require('../../Module/DatabaseModule.js').DatabaseModule;
var ResponseGeneratorModule  = require('../../Module/ResponseGeneratorModule.js').ResponseGeneratorModule;
var UtilityModule  = require('../../Module/UtilityModule.js').UtilityModule;
var UserModel = require('../../Model/UserModel.js').UserModel;
var MessageModel = require('../../Model/MessageModel.js').MessageModel;

var TAG = 'NotificationController';

var NotificationController = function(io, socket) {

	LogModule.logConstructor(TAG);

	var self = this;
	self.io = io;
	self.socket = socket;
	self.user = new UserModel();
	self.message = new MessageModel();

	self.on = function(data) {
		LogModule.log(TAG, 'on');
		LogModule.log(TAG, 'Notification Data', data);
		var sessionId = UtilityModule.sanitize(data[ApiModule.KEY.SESSION_ID]);
		self.user.sessionLogin(sessionId, self.login);
	};

	self.login = function() {
		LogModule.log(TAG, 'login');
		if(self.user.isLoggedIn()) {
			self.message.senderId = self.user.id;
			self.message.getMessageNotifications(self.notifications);
		} else {
			self.emitFailure(ResponseGeneratorModule.generateInvalidSessionResponse());
		}
	};

	self.notifications = function(success, notifs) {
		LogModule.log(TAG, 'notifications');
		if(success) {
			self.emitSuccess(ResponseGeneratorModule.generateNotificationsSuccessResponse(notifs));
		} else {
			self.emitFailure(ResponseGeneratorModule.generateNotificationsFailedResponse());
		}
	};

	self.emitSuccess = function(data) {
		LogModule.log(TAG, 'emitSuccess');
		LogModule.log(TAG, 'Notification Response', data);
		self.io.to(self.user.id).emit(SocketModule.SOCKET_EVENT.NOTIFICATION, data);
	};

	self.emitFailure = function(data) {
		LogModule.log(TAG, 'emitFailure');
		LogModule.log(TAG, 'Notification Response', data);
		self.socket.emit(SocketModule.SOCKET_EVENT.NOTIFICATION, data);
	};

};

exports.NotificationController = NotificationController;