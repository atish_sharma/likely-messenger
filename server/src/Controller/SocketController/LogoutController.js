var LogModule = require('../../Module/LogModule.js').LogModule;
var SocketModule  = require('../../Module/SocketModule.js').SocketModule;
var ApiModule = require('../../Module/ApiModule.js').ApiModule;
var ResponseGeneratorModule  = require('../../Module/ResponseGeneratorModule.js').ResponseGeneratorModule;
var UtilityModule  = require('../../Module/UtilityModule.js').UtilityModule;
var UserModel = require('../../Model/UserModel.js').UserModel;

var TAG = 'LogoutController';

var LogoutController = function(socket) {

	LogModule.logConstructor(TAG);

	var self = this;
	self.socket = socket;
	self.user = new UserModel();
	self.userId = null;

	self.on = function(data) {
		LogModule.log(TAG, 'on');
		LogModule.log(TAG, 'Logout Data', data);
		var sessionId = UtilityModule.sanitize(data[ApiModule.KEY.SESSION_ID]);
		self.user.sessionLogin(sessionId, self.login);
	};

	self.login = function() {
		LogModule.log(TAG, 'login');
		if(self.user.isLoggedIn()) {
			self.userId = self.user.id;
			self.user.logout(self.logout);
		} else {
			self.emit(ResponseGeneratorModule.generateInvalidSessionResponse());
		}
	};

	self.logout = function() {
		LogModule.log(TAG, 'logout');
		if(self.user.isLoggedIn()) {
			self.emit(ResponseGeneratorModule.generateLogoutFailedResponse());
		} else {
			self.socket.leave(self.userId);
			self.emit(ResponseGeneratorModule.generateLogoutSuccessResponse());
		}
	};

	self.emit = function(data) {
		LogModule.log(TAG, 'emit');
		LogModule.log(TAG, 'Logout Response', data);
		self.socket.emit(SocketModule.SOCKET_EVENT.LOGOUT, data);
	};

};

exports.LogoutController = LogoutController;