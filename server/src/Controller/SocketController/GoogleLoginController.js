var LogModule = require('../../Module/LogModule.js').LogModule;
var SocketModule  = require('../../Module/SocketModule.js').SocketModule;
var UtilityModule  = require('../../Module/UtilityModule.js').UtilityModule;
var ApiModule = require('../../Module/ApiModule.js').ApiModule;
var ResponseGeneratorModule  = require('../../Module/ResponseGeneratorModule.js').ResponseGeneratorModule;
var GoogleLoginService = require('../../Service/GoogleLoginService.js').GoogleLoginService;
var UserModel = require('../../Model/UserModel.js').UserModel;

var TAG = 'GoogleLoginController';

var GoogleLoginController = function(socket) {

	LogModule.logConstructor(TAG);

	var self = this;
	self.socket = socket;
	self.user = new UserModel();

	self.on = function(data) {
		LogModule.log(TAG, 'on');
		LogModule.log(TAG, 'Google Login Data', data);
		var tokenId = UtilityModule.sanitize(data[ApiModule.KEY.GOOGLE_AUTH_TOKEN_ID]);
		GoogleLoginService.login(tokenId, self.login);
	};

	self.login = function(success, googleId, name, email, picture) {
		LogModule.log(TAG, 'login');
		if(success) {
			self.user.googleLogin(googleId, name, email, picture, function() {
				if(self.user.isLoggedIn()) {
					self.user.getInfo(self.userInfo);
				} else {
					self.emit(ResponseGeneratorModule.generateLoginFailedResponse());
				}
			});
		} else {
			self.emit(ResponseGeneratorModule.generateLoginFailedResponse());
		}
	};

	self.userInfo = function(success, googleId, name, email, picture, sessionId, userId, signupTimestamp, sessionTimestamp, activeSessions) {
		LogModule.log(TAG, 'userInfo');
		if(success) {
			self.socket.join(self.user.id);
			self.emit(ResponseGeneratorModule.generateLoginSuccessResponse(googleId, name, email, picture, sessionId, userId, signupTimestamp, sessionTimestamp, activeSessions));
		} else {
			self.emit(ResponseGeneratorModule.generateLoginFailedResponse());
		}
	};

	self.emit = function(data) {
		LogModule.log(TAG, 'emit');
		LogModule.log(TAG, 'Google Login Response', data);
		self.socket.emit(SocketModule.SOCKET_EVENT.GOOGLE_LOGIN, data);
	};

};

exports.GoogleLoginController = GoogleLoginController;