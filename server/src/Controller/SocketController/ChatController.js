var LogModule = require('../../Module/LogModule.js').LogModule;
var SocketModule  = require('../../Module/SocketModule.js').SocketModule;
var ApiModule = require('../../Module/ApiModule.js').ApiModule;
var DatabaseModule = require('../../Module/DatabaseModule.js').DatabaseModule;
var ResponseGeneratorModule  = require('../../Module/ResponseGeneratorModule.js').ResponseGeneratorModule;
var UtilityModule  = require('../../Module/UtilityModule.js').UtilityModule;
var UserModel = require('../../Model/UserModel.js').UserModel;
var MessageModel = require('../../Model/MessageModel.js').MessageModel;

var TAG = 'ChatController';
var GET_MESSAGE_TYPE = {
	ANY : 'any',
	UNSEEN : 'unseen'
};

var ChatController = function(socket) {

	LogModule.logConstructor(TAG);

	var self = this;
	self.socket = socket;
	self.user = new UserModel();
	self.message = new MessageModel();
	self.getMessageType = null;
	self.timestamp = null;
	self.limit = null;

	self.on = function(data) {
		LogModule.log(TAG, 'on');
		LogModule.log(TAG, 'Chat Data', data);
		var sessionId = UtilityModule.sanitize(data[ApiModule.KEY.SESSION_ID]);
		self.message.receiverId = UtilityModule.sanitize(data[ApiModule.KEY.RECEIVER_ID]);
		self.getMessageType = UtilityModule.sanitize(data[ApiModule.KEY.GET_MESSAGE_TYPE]);
		self.timestamp = UtilityModule.sanitize(data[ApiModule.KEY.CHAT_TIMESTAMP]);
		self.limit = UtilityModule.sanitize(data[ApiModule.KEY.LIMIT]);
		self.user.sessionLogin(sessionId, self.login);
	};

	self.login = function() {
		LogModule.log(TAG, 'login');
		if(self.user.isLoggedIn()) {
			self.user.getUserInfo(self.message.receiverId, function(success, name, email, googleId, userId, picture, sessions) {
				if(success) {
					self.message.senderId = self.user.id;
					self.message.receiverId = userId;
					if(self.getMessageType == GET_MESSAGE_TYPE.ANY) {
						self.message.getMessages(self.timestamp, self.limit, self.chat);
					} else if(self.getMessageType == GET_MESSAGE_TYPE.UNSEEN) {
						self.message.getUnseenMessages(self.chat);
					} else {
						self.emit(ResponseGeneratorModule.generateChatFailedResponse());						
					}
				} else {
					self.emit(ResponseGeneratorModule.generateChatFailedResponse());
				}
			});
		} else {
			self.emit(ResponseGeneratorModule.generateInvalidSessionResponse());
		}
	};

	self.chat = function(success, senderId, receiverId, messages) {
		LogModule.log(TAG, 'chat');
		if(success) {
			self.emit(ResponseGeneratorModule.generateChatSuccessResponse(senderId, receiverId, messages));
		} else {
			self.emit(ResponseGeneratorModule.generateChatFailedResponse());
		}
	};

	self.emit = function(data) {
		LogModule.log(TAG, 'emit');
		LogModule.log(TAG, 'Chat Response', data);
		self.socket.emit(SocketModule.SOCKET_EVENT.CHAT, data);
	};

};

exports.ChatController = ChatController;