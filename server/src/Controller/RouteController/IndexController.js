var Path = require('path');
var LogModule = require('../../Module/LogModule.js').LogModule;

var TAG = 'IndexController';
var PATH = '/../../app/index.html';

var IndexController = function(app) {

	LogModule.logConstructor(TAG);

	var self = this;
	self.app = app;

	self.route = function(req, res) {
		LogModule.log(TAG, 'route');
		var path = Path.resolve(PATH);
		LogModule.log(TAG, 'Path -> ' + path);
		res.redirect(path);
	};

};

exports.IndexController = IndexController;