var LogModule = require('../../Module/LogModule.js').LogModule;
var IndexController = require('./IndexController.js').IndexController;

var TAG = 'RouteController';

var RouteController = function(app) {
	LogModule.logInvocation(TAG);
	var indexController = new IndexController(app);
	var ROUTES = [
		{
			route : '/',
			action : indexController.route
		},
		{
			route : '/login',
			action : indexController.route
		}
	];
	for(var i=0;i<ROUTES.length;i++) {
		app.get(ROUTES[i].route, ROUTES[i].action);
	}
};

exports.RouteController = RouteController;