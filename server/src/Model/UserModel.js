var LogModule = require('../Module/LogModule.js').LogModule;
var UtilityModule = require('../Module/UtilityModule.js').UtilityModule;
var DatabaseModule = require('../Module/DatabaseModule.js').DatabaseModule;

var TAG = 'UserModel';
var DEFAULT_USER_SEARCH_LIMIT = 10;
var MAX_USER_SEARCH_LIMIT = 20;

var UserModel = function() {

	LogModule.logConstructor(TAG);

	var self = this;
	self.db = DatabaseModule.getDatabase();
	self.id = null;
	self.sessionId = null;

	self.googleLogin = function(googleId, name, email, picture, next) {
		LogModule.log(TAG, 'googleLogin');
		self.findGoogleId(googleId, function(found) {
			if(found) {
				var users = self.db.get(DatabaseModule.TABLE.USERS);
				var data = {};
				data[DatabaseModule.COLUMN.GOOGLE_ID] = googleId;
				data[DatabaseModule.COLUMN.NAME] = name;
				data[DatabaseModule.COLUMN.EMAIL] = UtilityModule.trimAndLowerCase(email);
				data[DatabaseModule.COLUMN.PICTURE] = picture;
				var query = {};
				query[DatabaseModule.COLUMN.GOOGLE_ID] = googleId;
				users.update(query, { $set : data }, function(err, res) {
					if(err == null) {
						users.find(query, function(err, docs) {
							if(err == null) {
								self.id = docs[0][DatabaseModule.COLUMN.ID];
								self.createSession(next);
							} else {
								LogModule.log(TAG, err);
								next();
							}
				        });
					} else {
						LogModule.log(TAG, err);
						next();
					}
				});
			} else {
				self.signup(googleId, name, email, picture, function(success) {
					if(success) {
						self.googleLogin(googleId, name, email, picture, next);
					} else {
						next();
					}
				});
			}
		});
	};

	self.sessionLogin = function(sessionId, next) {
		LogModule.log(TAG, 'sessionLogin');
		var sessions = self.db.get(DatabaseModule.TABLE.SESSIONS);
		var query = {};
		query[DatabaseModule.COLUMN.ID] = sessionId;
		try {
			sessions.find(query, function(err, docs) {
				if(err == null) {
					if(docs.length > 0) {
						var doc = docs[0];
						self.id = doc[DatabaseModule.COLUMN.USER_ID];
						self.sessionId = doc[DatabaseModule.COLUMN.ID];
					} 
				} else {
					LogModule.log(TAG, err);
				}
				next();
			});
		} catch(err) {
			LogModule.log(TAG, err);
			next();
		}
	};

	self.getInfo = function(next) {
		LogModule.log(TAG, 'getInfo');
		var users = self.db.get(DatabaseModule.TABLE.USERS);
		var sessions = self.db.get(DatabaseModule.TABLE.SESSIONS);
		var query = {};
		var userId = self.id;
		query[DatabaseModule.COLUMN.ID] = userId;
		try {
			users.find(query, function(err, docs) {
				if(err == null) {
					var doc = docs[0];
					var name = doc[DatabaseModule.COLUMN.NAME];
					var googleId = doc[DatabaseModule.COLUMN.GOOGLE_ID];
					var email = doc[DatabaseModule.COLUMN.EMAIL];
					var picture = doc[DatabaseModule.COLUMN.PICTURE];
					var signupTimestamp = doc[DatabaseModule.COLUMN.SIGNUP_TIMESTAMP];
					query = {};
					query[DatabaseModule.COLUMN.USER_ID] = userId;
					sessions.find(query, function(err, docs) {
						if(err == null) {
							var sessionCount = docs.length;
							var sessionId = self.sessionId;
							query = {};
							query[DatabaseModule.COLUMN.ID] = sessionId;
							sessions.find(query, function(err, docs) {
								if(err == null) {
									doc = docs[0];
									var sessionTimestamp = doc[DatabaseModule.COLUMN.SESSION_TIMESTAMP];
									next(true, googleId, name, email, picture, sessionId, userId, signupTimestamp, sessionTimestamp, sessionCount);
								} else {
									LogModule.log(TAG, err);
									next(false);
								}
							});
						} else {
							LogModule.log(TAG, err);
							next(false);
						}
					});
				} else {
					LogModule.log(TAG, err);
					next(false);
				}
			});
		} catch(err) {
			LogModule.log(TAG, err);
			next(false);
		}
	};

	self.logout = function(next) {
		LogModule.log(TAG, 'logout');
		var sessions = self.db.get(DatabaseModule.TABLE.SESSIONS);
		var query = {};
		query[DatabaseModule.COLUMN.ID] = self.sessionId;
		sessions.remove(query, function(err, res) {
			if(err == null) {
				self.clear();
			} else {
				LogModule.log(TAG, err);
			}
			next();
		});
	};

	self.isLoggedIn = function() {
		LogModule.log(TAG, 'isLoggedIn');
		return self.sessionId != null;
	};

	self.findGoogleId = function(googleId, next) {
		LogModule.log(TAG, 'findGoogleId');
		var users = self.db.get(DatabaseModule.TABLE.USERS);
		var query = {};
		query[DatabaseModule.COLUMN.GOOGLE_ID] = googleId;
		users.find(query, function(err, docs) {
			if(err == null) {
				if(docs.length > 0) {
					next(true);
				} else {
					next(false);
				}
			} else {
				LogModule.log(TAG, err);
				next(false);
			}
		});
	};

	self.createSession = function(next) {
		LogModule.log(TAG, 'createSession');
		var sessions = self.db.get(DatabaseModule.TABLE.SESSIONS);
		var data = {};
		data[DatabaseModule.COLUMN.USER_ID] = self.id;
		data[DatabaseModule.COLUMN.SESSION_TIMESTAMP] = UtilityModule.time();
		sessions.insert(data, function(err, doc) {
			if(err == null) {
				self.sessionId = doc[DatabaseModule.COLUMN.ID];
			} else {
				LogModule.log(TAG, err);
			}
			next();
		});
	};

	self.clearSessions = function(next) {
		LogModule.log(TAG, 'clearSessions');
		var sessions = self.db.get(DatabaseModule.TABLE.SESSIONS);
		var query = {};
		query[DatabaseModule.COLUMN.USER_ID] = self.id;
		sessions.remove(query, function(err, res) {
			if(err == null) {
				self.clear();
				next(true);
			} else {
				LogModule.log(TAG, err);
				next(false);
			}
		});
	};

	self.signup = function(googleId, name, email, picture, next) {
		LogModule.log(TAG, 'signup');
		var users = self.db.get(DatabaseModule.TABLE.USERS);
		var data = {};
		data[DatabaseModule.COLUMN.GOOGLE_ID] = googleId;
		data[DatabaseModule.COLUMN.NAME] = name;
		data[DatabaseModule.COLUMN.EMAIL] = UtilityModule.trimAndLowerCase(email);
		data[DatabaseModule.COLUMN.PICTURE] = picture;
		data[DatabaseModule.COLUMN.SIGNUP_TIMESTAMP] = UtilityModule.time();
		users.insert(data, function(err, doc) {
			if(err == null) {
				next(true);
			} else {
				LogModule.log(TAG, err);
				next(false);
			}
		});
	};

	self.userSearch = function(email, limiter, next) {
		LogModule.log(TAG, 'userSearch');
		if(!UtilityModule.isUsableString(email) || email == '') {
			next(false);
			return;
		}
		self.getInfo(function(success, googleId, name, userEmail, picture, sessionId, userId, signupTimestamp, sessionTimestamp, sessionCount) {
			if(success) {
				limiter = parseInt(limiter);
				if(!UtilityModule.isUsableNumber(limiter) || limiter == 0) {
					limiter = DEFAULT_USER_SEARCH_LIMIT;
				}
				if(limiter > MAX_USER_SEARCH_LIMIT) {
					limiter = MAX_USER_SEARCH_LIMIT;
				}
				email = UtilityModule.trimAndLowerCase(email);
				var searchTerm = new RegExp('.*' + email + '.*');
				var users = self.db.get(DatabaseModule.TABLE.USERS);
				var query = {};
				query[DatabaseModule.COLUMN.EMAIL] = {};
				query[DatabaseModule.COLUMN.EMAIL]['$regex'] = searchTerm;
				query[DatabaseModule.COLUMN.EMAIL]['$ne'] = userEmail;
				var sorter = {};
				sorter[DatabaseModule.COLUMN.NAME] = 1;
				users.find(query, {
					limit : limiter,
					sort : sorter
				}, function(err, docs) {
					if(err == null) {
						next(true, docs);
					} else {
						LogModule.log(TAG, err);
						next(false);
					}
				});
			} else {
				next(false);
			}
		});
	};

	self.getUserInfo = function(userId, next) {
		LogModule.log(TAG, 'getUserInfo');
		var users = self.db.get(DatabaseModule.TABLE.USERS);
		var query = {};
		query[DatabaseModule.COLUMN.ID] = userId;
		try {
			users.find(query, function(err, docs) {
				if(err == null) {
					if(docs.length > 0) {
						var doc = docs[0];
						var googleId = doc[DatabaseModule.COLUMN.GOOGLE_ID];
						var name = doc[DatabaseModule.COLUMN.NAME];
						var email = doc[DatabaseModule.COLUMN.EMAIL];
						var picture = doc[DatabaseModule.COLUMN.PICTURE];
						userId = doc[DatabaseModule.COLUMN.ID];
						var sessions = self.db.get(DatabaseModule.TABLE.SESSIONS);
						query = {};
						query[DatabaseModule.COLUMN.USER_ID] = userId;
						sessions.find(query, function(err, docs) {
							var sessionList = [];
							for(var i=0;i<docs.length;i++) {
								sessionList.push(docs[i][DatabaseModule.COLUMN.ID]);
							}
							if(err == null) {
								next(true, name, email, googleId, userId, picture, sessionList);
							} else {
								LogModule.log(TAG, err);
								next(false);
							}
						});
					} else {
						next(false);
					}
				} else {
					LogModule.log(TAG, err);
					next(false);
				}
			});
		} catch(err) {
			LogModule.log(TAG, err);
			next(false);
		}
	};

	self.clear = function() {
		LogModule.log(TAG, 'clear');
		self.id = null;
		self.sessionId = null;
	};

};

exports.UserModel = UserModel;