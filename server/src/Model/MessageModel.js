var WF = require('../../node_modules/word-freq');
var LogModule = require('../Module/LogModule.js').LogModule;
var UtilityModule = require('../Module/UtilityModule.js').UtilityModule;
var DatabaseModule = require('../Module/DatabaseModule.js').DatabaseModule;
var UserModel = require('./UserModel.js').UserModel;

var TAG = 'MessageModel';
var DEFAULT_MESSAGE_LIMIT = 100;
var MAX_MESSAGE_LIMIT = 200;
var MAX_TF_IDF_USERS_RES = 3;

var MessageModel = function() {

	LogModule.logConstructor(TAG);

	var self = this;
	self.db = DatabaseModule.getDatabase();
	self.senderId = null;
	self.receiverId = null;
	self.message = null;
	self.timestamp = null;
	self.notified = null; 
	self.id = null;

	self.save = function(next) {
		LogModule.log(TAG, 'save');
		if(UtilityModule.isUsableString(self.message) && self.message != '' && self.receiverId != self.senderId) {
			var messages = self.db.get(DatabaseModule.TABLE.MESSAGES);
			var data = {};
			data[DatabaseModule.COLUMN.SENDER_ID] = self.senderId;
			data[DatabaseModule.COLUMN.RECEIVER_ID] = self.receiverId;
			data[DatabaseModule.COLUMN.MESSAGE] = self.message;
			data[DatabaseModule.COLUMN.MESSAGE_TIMESTAMP] = self.timestamp = UtilityModule.time();
			data[DatabaseModule.COLUMN.NOTIFIED] = self.notified = false;
			messages.insert(data, function(err, doc) {
				if(err == null) {
					self.id = doc[DatabaseModule.COLUMN.ID];
					var res = self.stem(self.message);
					for(var i=0;i<res.length;i++) {
						self.updateFrequency(res[i].word, res[i].freq);
					}
					next(true);
				} else {
					LogModule.log(TAG, err);
					next(false);
				}
			});
		} else {
			next(false);
		}
	};

	self.updateFrequency = function(word, freq) {
		LogModule.log(TAG, 'updateFrequencey');
		if(UtilityModule.isUsableString(word)) {
			word = UtilityModule.trimAndLowerCase(word);
		} else {
			return;
		}
		if(word == '') {
			return;
		}
		var wordFrequency = self.db.get(DatabaseModule.TABLE.WORD_FREQUENCY);
		var query = {};
		query[DatabaseModule.COLUMN.USER_ID] = self.senderId;
		query[DatabaseModule.COLUMN.WORD] = word;
		wordFrequency.find(query, function(err, docs) {
			if(err == null) {
				var data = {};
				if(docs.length > 0) {
					data[DatabaseModule.COLUMN.FREQUENCY] = docs[0][DatabaseModule.COLUMN.FREQUENCY] + freq;
					wordFrequency.update(query, { $set : data }, function(err, res) {
						if(err != null) {
							LogModule.log(TAG, err);
						}
					});
				} else {
					data[DatabaseModule.COLUMN.FREQUENCY] = freq;
					data[DatabaseModule.COLUMN.USER_ID] = self.senderId;
					data[DatabaseModule.COLUMN.WORD] = word;
					wordFrequency.insert(data, function(err, res) {
						if(err != null) {
							LogModule.log(TAG, err);
						}
					});
				}
			} else {
				LogModule.log(TAG, err);
			}
		});
	};

	self.stem = function(sentence) {
		LogModule.log(TAG, 'stem');
		var res = [];
		if(UtilityModule.isUsableString(sentence)) {
			sentence = UtilityModule.trimAndLowerCase(sentence);
			if(sentence != '') {
				var freq = WF.freq(sentence, true, false);
				for(var word in freq) {
					if (freq.hasOwnProperty(word)) {
						var f = freq[word];
						res.push({
							'word' : word,
							'freq' : f
						})
					}
				}
			}
		}
		return res;
	};

	self.query = function(sentence, next) {
		LogModule.log(TAG, 'query');
		var res = self.stem(sentence);
		var tags = [];
		for(var i=0;i<res.length;i++) {
			tags.push(res[i]['word']);
		}
		var users = self.db.get(DatabaseModule.TABLE.USERS);
		var query = {};
		query[DatabaseModule.COLUMN.ID] = {};
		query[DatabaseModule.COLUMN.ID]['$ne'] = self.senderId;
		users.find(query, function(err, docs) {
			if(err == null) {
				for(var i=0;i<docs.length;i++) {
					docs[i]['score'] = 0.0;
				}
				self.queryWords(tags, docs, 0, function(success, users) {
					if(success) {
						users.sort(function(user1, user2) {
							if(user1['score'] > user2['score']) {
								return -1;
							} else if(user1['score'] < user2['score']) {
								return 1;
							}
							return 0;
						});
						var toppers = [];
						for(var i=0;i < users.length && i < MAX_TF_IDF_USERS_RES;i++) {
							toppers.push(users[i]);
						}
						next(true, tags, toppers);
					} else {
						next(false);
					}
				});
			} else {
				LogModule.log(TAG, err);
				next(false);
			}
		});
	};

	self.queryWords = function(words, users, pos, next) {
		LogModule.log(TAG, 'queryWords');
		if(pos == words.length) {
			next(true, users);
			return;
		}
		var word = words[pos];
		var userCount = users.length;
		self.getInverseDocumentFrequency(word, function(success, idf) {
			if(success) {
				var noError = true;
				for(var i=0;i<users.length;i++) {
					if(!noError) {
						return;
					}
					const CURR_USER = i;
					self.getTermFrequency(users[i][DatabaseModule.COLUMN.ID], word, function(success, tf) {
						if(success) {
							users[CURR_USER]['score'] += self.getTfIdf(tf, idf, users.length);
							userCount--;
							if(userCount == 0 && noError) {
								self.queryWords(words, users, pos + 1, next);
							}
						} else {
							if(noError) {
								noError = false;
								next(false);
							}
						}
					});
				}
			} else {
				next(false);
			}
		});
	};

	self.getTermFrequency = function(userId, word, next) {
		LogModule.log(TAG, 'getTermFrequency');
		var wordFrequency = self.db.get(DatabaseModule.TABLE.WORD_FREQUENCY);
		var query = {};
		LogModule.log(TAG, userId);
		LogModule.log(TAG, word);
		query[DatabaseModule.COLUMN.USER_ID] = userId;
		query[DatabaseModule.COLUMN.WORD] = word;
		wordFrequency.find(query, function(err, docs) {
			if(err == null) {
				var tf = 0;
				if(docs.length > 0) {
					tf = docs[0][DatabaseModule.COLUMN.FREQUENCY];
				}
				LogModule.log(TAG, 'TF for word "' + word + '" and userId ' + userId + ' -> ' + tf);
				next(true, tf);
			} else {
				LogModule.log(TAG, err);
				next(false);
			}
		});
	};

	self.getInverseDocumentFrequency = function(word, next) {
		LogModule.log(TAG, 'getInverseDocumentFrequency');
		var wordFrequency = self.db.get(DatabaseModule.TABLE.WORD_FREQUENCY);
		var query = {};
		query[DatabaseModule.COLUMN.WORD] = word;
		query[DatabaseModule.COLUMN.USER_ID] = {};
		query[DatabaseModule.COLUMN.USER_ID]['$ne'] = self.senderId;
		wordFrequency.find(query, function(err, docs) {
			if(err == null) {
				LogModule.log(TAG, 'IDF for ' + word + ' -> ' + docs.length);
				next(true, docs.length);
			} else {
				LogModule.log(TAG, err);
				next(false);
			}
		});
	};

	self.getTfIdf = function(tf, idf, len) {
		LogModule.log(TAG, 'getTfIdf');
		if(idf != 0) {
			idf = Math.log(len / idf);
		}
		tf = tf / len;
		var tfIdf = idf * tf;
		if(!UtilityModule.isUsableNumber(tfIdf)) {
			tfIdf = 0;
		}
		LogModule.log(TAG , 'TF-IDF -> ' + tfIdf);
		return tfIdf;
	};
 
	self.getUnseenMessages = function(next) {
		LogModule.log(TAG, 'getUnseenMessages');
		var messages = self.db.get(DatabaseModule.TABLE.MESSAGES);
		var query = {};
		query[DatabaseModule.COLUMN.SENDER_ID] = self.receiverId;
		query[DatabaseModule.COLUMN.RECEIVER_ID] = self.senderId;
		query[DatabaseModule.COLUMN.NOTIFIED] = false;
		var sorter = {};
		sorter[DatabaseModule.COLUMN.MESSAGE_TIMESTAMP] = -1;
		messages.find(query, { sort : sorter }, function(err, docs) {
			if(err == null) {
				next(true, self.senderId, self.receiverId, docs);
			} else {
				LogModule.log(TAG, err);
				next(false);
			}
		});
	};

	self.getMessageNotifications = function(next) {
		LogModule.log(TAG, 'getMessageNotifications');
		var messages = self.db.get(DatabaseModule.TABLE.MESSAGES);
		var query = {};
		query[DatabaseModule.COLUMN.RECEIVER_ID] = self.senderId;
		query[DatabaseModule.COLUMN.NOTIFIED] = false;
		var sorter = {};
		sorter[DatabaseModule.COLUMN.MESSAGE_TIMESTAMP] = -1;
		messages.find(query, { sort : sorter }, function(err, docs) {
			if(err == null) {
				var notifications = {};
				for(var i=0;i<docs.length;i++) {
					var doc = docs[i];
					var senderId = doc[DatabaseModule.COLUMN.SENDER_ID];
					if(!notifications.hasOwnProperty(senderId)) {
						notifications[senderId] = {
							message_count : 0,
							timestamp : 0,
							info : null
						};
					}
					notifications[senderId].message_count++;
					if(notifications[senderId].timestamp < doc[DatabaseModule.COLUMN.MESSAGE_TIMESTAMP]) {
						notifications[senderId].timestamp = doc[DatabaseModule.COLUMN.MESSAGE_TIMESTAMP];
					}
				}
				var notifs = [];
				for(var notif in notifications) {
					if (notifications.hasOwnProperty(notif)) {
						var n = {};
						n['sender_id'] = notif;
						n['data'] = notifications[notif]; 
						notifs.push(n);
					}
				}
				var notifCount = notifs.length;
				if(notifCount == 0) {
					next(true, []);
					return
				}
				var user = new UserModel();
				var noError = true;
				for(var i=0;i<notifs.length;i++) {
					if(!noError) {
						return;
					}
					const CURR_NOTIF = i;
					user.getUserInfo(notifs[i].sender_id, function(success, name, email, googleId, userId, picture, sessions) {
						if(success) {
							notifs[CURR_NOTIF].data.info = {
								'email' : email,
								'name' : name,
								'google_id' : googleId,
								'user_id' : userId,
								'picture' : picture
							};
							notifCount--;
							if(notifCount == 0) {
								next(true, notifs);
							}
						} else {
							noError = false;
							next(false);
						}
					});
				}
			} else {
				LogModule.log(TAG, err);
				next(false);
			}
		});
	};

	self.getMessages = function(timestamp, limiter, next) {
		LogModule.log(TAG, 'getMessages');
		limiter = parseInt(limiter);
		if(!UtilityModule.isUsableNumber(limiter) || limiter == 0) {
			limiter = DEFAULT_MESSAGE_LIMIT;
		}
		if(limiter > MAX_MESSAGE_LIMIT) {
			limiter = MAX_MESSAGE_LIMIT;
		}
		timestamp = parseInt(timestamp);
		if(!UtilityModule.isUsableNumber(timestamp) || timestamp == 0) {
			timestamp = UtilityModule.time();
		}
		var messages = self.db.get(DatabaseModule.TABLE.MESSAGES);
		var query = {};
		var query1 = {};
		query1[DatabaseModule.COLUMN.SENDER_ID] = self.senderId;
		var query2 = {};
		query2[DatabaseModule.COLUMN.RECEIVER_ID] = self.receiverId;
		var query3 = {};
		query3[DatabaseModule.COLUMN.SENDER_ID] = self.receiverId;
		var query4 = {};
		query4[DatabaseModule.COLUMN.RECEIVER_ID] = self.senderId;
		var query5 = {};
		query5['$and'] = [];
		query5['$and'].push(query1);
		query5['$and'].push(query2);
		var query6 = {};
		query6['$and'] = [];
		query6['$and'].push(query3);
		query6['$and'].push(query4);
		query['$or'] = [];
		query['$or'].push(query5);
		query['$or'].push(query6);
		query[DatabaseModule.COLUMN.MESSAGE_TIMESTAMP] = {};
		query[DatabaseModule.COLUMN.MESSAGE_TIMESTAMP]['$lt'] = timestamp; 
		var sorter = {};
		sorter[DatabaseModule.COLUMN.MESSAGE_TIMESTAMP] = -1;
		messages.find(query, { limit : limiter, sort : sorter }, function(err, docs) {
			if(err == null) {
				next(true, self.senderId, self.receiverId, docs);
			} else {
				LogModule.log(TAG, err);
				next(false);
			}
		});
	};

	self.setMessagesSeen = function(next) {
		LogModule.log(TAG, 'setMessagesSeen');
		var messages = self.db.get(DatabaseModule.TABLE.MESSAGES);
		var query = {};
		query[DatabaseModule.COLUMN.SENDER_ID] = self.receiverId;
		query[DatabaseModule.COLUMN.RECEIVER_ID] = self.senderId;
		query[DatabaseModule.COLUMN.NOTIFIED] = false;
		var data = {};
		data[DatabaseModule.COLUMN.NOTIFIED] = true;
		messages.update(query, { $set : data }, { multi : true }, function(err, res) {
			if(err == null) {
				next(true);
			} else {
				LogModule.log(TAG, err);
				next(false);
			}
		});
	};

};

exports.MessageModel = MessageModel;