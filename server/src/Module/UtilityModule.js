var shell = require('../../node_modules/shelljs');
var sanitize = require('../../node_modules/mongo-sanitize');
var _ = require('../../node_modules/underscore');
var LogModule = require('./LogModule.js').LogModule;

var TAG = 'UtilityModule';

var UtilityModule = {

	time : function() {
		LogModule.log(TAG, 'time');
		return new Date().getTime();
	},

	isEmpty : function(json) {
		LogModule.log(TAG, 'isEmpty');
		return _.isEmpty(json);
	},

	sanitize : function(str) {
		LogModule.log(TAG, 'sanitize');
		if(this.isUsableString(str)){
			return sanitize(str.trim());
		}
		return null;
	},

	isUsable : function(obj) {
		LogModule.log(TAG, 'isUsable');
		return !_.isUndefined(obj) && !_.isNull(obj);
	},

	isUsableNumber : function(num) {
		LogModule.log(TAG, 'isUsableNumber');
		return this.isUsable(num) && _.isNumber(num) && !_.isNaN(num);
	},

	isUsableString : function(str) {
		LogModule.log(TAG, 'isUsableString');
		return this.isUsable(str) && _.isString(str);
	},

	trim : function(str) {
		LogModule.log(TAG, 'trim');
		if(this.isUsableString(str)) {
			return str.trim();
		}
		return null;
	},

	lowerCase : function(str) {
		LogModule.log(TAG, 'lowerCase');
		if(this.isUsableString(str)) {
			return str.toLowerCase();
		}
		return null;
	},

	trimAndLowerCase : function(str) {
		LogModule.log(TAG, 'trimAndLowerCase');
		if(this.isUsableString(str)) {
			return str.trim().toLowerCase();
		}
		return null;
	},

	exec : function(command, next) {
		LogModule.log(TAG, 'exec');
		shell.exec(command, function(status, output) {
			next(status, output);
		});	
	},

	cloneJson : function(json) {
		LogModule.log(TAG, 'cloneJson');
		if(this.isUsable(json)){
			return JSON.parse(JSON.stringify(json)); 
		}
		return {};
	},

	isObject : function(obj) {
		LogModule.log(TAG, 'isObject');
		return _.isObject(obj);
	},

	isArray : function(obj) {
		LogModule.log(TAG, 'isArray');
		return _.isArray(obj);
	},

	parseJson : function(str) {
		LogModule.log(TAG, 'parseJson');
		var json = null;
		try {
			json = JSON.parse(str);
		} catch(err) {
			LogModule.log(TAG, err);
		}
		return json;
	}

};

LogModule.logConstructor(TAG);

exports.UtilityModule = UtilityModule;