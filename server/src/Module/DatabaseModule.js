var LogModule = require('./LogModule.js').LogModule;

var TAG = 'DatabaseModule';

var DatabaseModule = {

	TABLE : {
		USERS : 'users',
		SESSIONS : 'sessions',
		MESSAGES : 'messages',
		WORD_FREQUENCY : 'word_frequency'
	},
	
	COLUMN : {
		ID : '_id',
		GOOGLE_ID : 'google_id',
		NAME : 'name',
		EMAIL : 'email',
		PICTURE : 'profile_pic_url',
		SIGNUP_TIMESTAMP : 'signup_timestamp',
		SESSION_ID : 'session_id',
		SESSION_TIMESTAMP : 'session_timestamp',
		USER_ID : 'user_id',
		MESSAGE : 'message',
		SENDER_ID : 'sender_id',
		RECEIVER_ID : 'receiver_id',
		MESSAGE_ID : 'message_id',
		MESSAGE_TIMESTAMP : 'message_timestamp',
		NOTIFIED : 'notified',
		WORD : 'word',
		FREQUENCY : 'frequency'
	},

	database : null,

	setDatabase : function(database) {
		LogModule.log(TAG, 'setDatabase');
		this.database = database;
	},

	getDatabase : function() {
		LogModule.log(TAG, 'getDatabase');
		return this.database;
	}

};

LogModule.logConstructor(TAG);

exports.DatabaseModule = DatabaseModule;