var LogModule = require('./LogModule.js').LogModule;

var TAG = 'SocketModule';

var SocketModule = {

	SOCKET_EVENT : {
		CONNECT : 'connect',
		DISCONNECT : 'disconnect',
		GOOGLE_LOGIN : 'google_login',
		SESSION_LOGIN : 'session_login',
		LOGOUT : 'logout',
		SESSION_CLEARANCE : 'clear_sessions',
		USER_SEARCH : 'user_search',
		MESSAGE : 'message',
		MESSAGE_STATUS : 'message_status',
		SUGGESTIONS : 'suggestions',
		NOTIFICATION : 'notification',
		CHAT_SEEN : 'chat_seen',
		CHAT : 'chat'
	}

};

LogModule.logConstructor(TAG);

exports.SocketModule = SocketModule;