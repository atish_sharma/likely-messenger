var LogModule = require('./LogModule.js').LogModule;
var ApiModule  = require('./ApiModule.js').ApiModule;
var DatabaseModule  = require('./DatabaseModule.js').DatabaseModule;

var TAG = 'ResponseGeneratorModule';

var ResponseGeneratorModule = {

	generateLoginSuccessResponse : function(googleId, name, email, picture, sessionId, userId, signupTimestamp, sessionTimestamp, activeSessions) {
		LogModule.log(TAG, 'generateLoginSuccessResponse');
		var res = {};
		res[ApiModule.KEY.SUCCESS] = true;
		res[ApiModule.KEY.GOOGLE_ID] = googleId;
		res[ApiModule.KEY.NAME] = name;
		res[ApiModule.KEY.EMAIL] = email;
		res[ApiModule.KEY.PROFILE_PIC_URL] = picture;
		res[ApiModule.KEY.SESSION_ID] = sessionId;
		res[ApiModule.KEY.USER_ID] = userId;
		res[ApiModule.KEY.SIGNUP_TIMESTAMP] = signupTimestamp;
		res[ApiModule.KEY.SESSION_TIMESTAMP] = sessionTimestamp;
		res[ApiModule.KEY.ACTIVE_SESSIONS] = activeSessions;
		return res;
	},

	generateLoginFailedResponse : function() {
		LogModule.log(TAG, 'generateLoginFailedResponse');
		var res = {};
		res[ApiModule.KEY.SUCCESS] = false;
		res[ApiModule.KEY.ERROR] = ApiModule.ERROR.GOOGLE_LOGIN_FAILED;
		return res;
	},

	generateInvalidSessionResponse : function() {
		LogModule.log(TAG, 'generateInvalidSessionResponse');
		var res = {};
		res[ApiModule.KEY.SUCCESS] = false;
		res[ApiModule.KEY.ERROR] = ApiModule.ERROR.INVALID_SESSION;
		return res;
	},

	generateLogoutFailedResponse : function() {
		LogModule.log(TAG, 'generateLogoutFailedResponse');
		var res = {};
		res[ApiModule.KEY.SUCCESS] = false;
		res[ApiModule.KEY.ERROR] = ApiModule.ERROR.LOGOUT_FAILED;
		return res;
	},

	generateLogoutSuccessResponse : function() {
		LogModule.log(TAG, 'generateLogoutSuccessResponse');
		var res = {};
		res[ApiModule.KEY.SUCCESS] = true;
		return res;
	},

	generateSessionClearanceFailedResponse : function() {
		LogModule.log(TAG, 'generateSessionClearanceFailedResponse');
		var res = {};
		res[ApiModule.KEY.SUCCESS] = false;
		res[ApiModule.KEY.ERROR] = ApiModule.ERROR.SESSION_CLEARANCE_FAILED;
		return res;
	},

	generateSessionClearanceSuccessResponse : function() {
		LogModule.log(TAG, 'generateSessionClearanceSuccessResponse');
		var res = {};
		res[ApiModule.KEY.SUCCESS] = true;
		return res;
	},

	generateUserSearchFailedResponse : function() {
		LogModule.log(TAG, 'generateUserSearchFailedResponse');
		var res = {};
		res[ApiModule.KEY.SUCCESS] = false;
		res[ApiModule.KEY.ERROR] = ApiModule.ERROR.USER_SEARCH_FAILED;
		return res;
	},

	generateUserSearchSuccessResponse : function(users) {
		LogModule.log(TAG, 'generateUserSearchSuccessResponse');
		var res = {};
		res[ApiModule.KEY.SUCCESS] = true;
		res[ApiModule.KEY.USERS] = users;
		return res;
	},

	generateMessageFailedResponse : function() {
		LogModule.log(TAG, 'generateMessageFailedResponse');
		var res = {};
		res[ApiModule.KEY.SUCCESS] = false;
		res[ApiModule.KEY.ERROR] = ApiModule.ERROR.MESSAGE_FAILED;
		return res;
	},

	generateMessageSuccessResponse : function() {
		LogModule.log(TAG, 'generateMessageSuccessResponse');
		var res = {};
		res[ApiModule.KEY.SUCCESS] = true;
		return res;
	},

	generateNotificationsFailedResponse : function() {
		LogModule.log(TAG, 'generateNotificationsFailedResponse');
		var res = {};
		res[ApiModule.KEY.SUCCESS] = false;
		res[ApiModule.KEY.ERROR] = ApiModule.ERROR.NOTIFICATION_FAILED;
		return res;
	},

	generateNotificationsSuccessResponse : function(notifs) {
		LogModule.log(TAG, 'generateNotificationsSuccessResponse');
		var res = {};
		res[ApiModule.KEY.SUCCESS] = true;
		res[ApiModule.KEY.NOTIFICATIONS] = [];
		for(var i=0;i<notifs.length;i++) {
			var data = notifs[i].data;
			var info = data.info;
			var notif = {};
			notif[ApiModule.KEY.MESSAGE_COUNT] = data.message_count;
			notif[ApiModule.KEY.LATEST_MESSAGE_TIMESTAMP] = data.timestamp;
			notif[ApiModule.KEY.NAME] = info.name;
			notif[ApiModule.KEY.EMAIL] = info.email;
			notif[ApiModule.KEY.GOOGLE_ID] = info.google_id;
			notif[ApiModule.KEY.PROFILE_PIC_URL] = info.picture;
			notif[ApiModule.KEY.USER_ID] = info.user_id;
			res[ApiModule.KEY.NOTIFICATIONS].push(notif);
		}
		res[ApiModule.KEY.NOTIFICATIONS].sort(function(notif1, notif2) {
			if(notif1[ApiModule.KEY.LATEST_MESSAGE_TIMESTAMP] > notif2[ApiModule.KEY.LATEST_MESSAGE_TIMESTAMP]) {
				return -1;
			}
			if(notif1[ApiModule.KEY.LATEST_MESSAGE_TIMESTAMP] < notif2[ApiModule.KEY.LATEST_MESSAGE_TIMESTAMP]) {
				return 1;
			}
			return 0;
		});
		return res;
	},

	generateChatSeenFailedResponse : function() {
		LogModule.log(TAG, 'generateChatSeenFailedResponse');
		var res = {};
		res[ApiModule.KEY.SUCCESS] = false;
		res[ApiModule.KEY.ERROR] = ApiModule.ERROR.CHAT_SEEN_FAILED;
		return res;
	},

	generateChatSeenSuccessResponse : function() {
		LogModule.log(TAG, 'generateChatSeenSuccessResponse');
		var res = {};
		res[ApiModule.KEY.SUCCESS] = true;
		return res;
	},

	generateChatSuccessResponse : function(senderId, receiverId, messages) {
		LogModule.log(TAG, 'generateChatSuccessResponse');
		var res = {};
		res[ApiModule.KEY.SUCCESS] = true;
		res[ApiModule.KEY.SENDER_ID] = senderId;
		res[ApiModule.KEY.RECEIVER_ID] = receiverId;
		res[ApiModule.KEY.MESSAGES] = [];
		for(var i=0;i<messages.length;i++) {
			var message = messages[i];
			var msg = {};
			msg[ApiModule.KEY.SENDER_ID] = message[DatabaseModule.COLUMN.SENDER_ID];
			msg[ApiModule.KEY.RECEIVER_ID] = message[DatabaseModule.COLUMN.RECEIVER_ID];
			msg[ApiModule.KEY.MESSAGE] = message[DatabaseModule.COLUMN.MESSAGE];
			msg[ApiModule.KEY.MESSAGE_TIMESTAMP] = message[DatabaseModule.COLUMN.MESSAGE_TIMESTAMP];
			msg[ApiModule.KEY.MESSAGE_ID] = message[DatabaseModule.COLUMN.ID];
			msg[ApiModule.KEY.SEEN] = message[DatabaseModule.COLUMN.NOTIFIED];
			res[ApiModule.KEY.MESSAGES].push(msg);
		}
		return res;
	},

	generateChatFailedResponse : function() {
		LogModule.log(TAG, 'generateChatFailedResponse');
		var res = {};
		res[ApiModule.KEY.SUCCESS] = false;
		res[ApiModule.KEY.ERROR] = ApiModule.ERROR.CHAT_FAILED;
		return res;
	}

};

LogModule.logConstructor(TAG);

exports.ResponseGeneratorModule = ResponseGeneratorModule;