require('../../node_modules/node-env-file')('env');
var _ = require('../../node_modules/underscore');

var TAG = 'LogModule';

var LogModule = {

	INVOCATION : '<<invocation>>',
	CONSTRUCTOR : '<<constructor>>',

	log : function(tag, data, json) {
		if(process.env.SERVER_STATE == 'dev') {
			console.log(tag, data);
			if(!_.isUndefined(json) && !_.isNull(json)) { 
				console.log('........................................');
				console.log(JSON.stringify(json, null, 2));
			}
			console.log('----------------------------------------');
		}
	},

	logInvocation : function(tag) {
		this.log(tag, this.INVOCATION);
	},

	logConstructor : function(tag) {
		this.log(tag, this.CONSTRUCTOR);
	}

};

LogModule.logConstructor(TAG);

exports.LogModule = LogModule;