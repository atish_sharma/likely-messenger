require('../../node_modules/node-env-file')('env');
var request = require('../../node_modules/request');
var LogModule = require('../Module/LogModule.js').LogModule;
var UtilityModule = require('../Module/UtilityModule.js').UtilityModule;

var TAG = 'GoogleLoginService';

var GoogleLoginService = {

	CLIENT_ID : process.env.GOOGLE_CLIENT_ID,
	CLIENT_SECRET : process.env.GOOGLE_CLIENT_SECRET,
	GOOGLE_AUTH_URL : 'https://www.googleapis.com/oauth2/v3/tokeninfo?id_token=',
	GOOGLE_AUTH_API : {
		EMAIL : 'email',
		NAME : 'name',
		PROFILE_PIC_URL : 'picture',
		GOOGLE_ID : 'sub',
		CLIENT_ID : 'aud' 
	},

	login : function(tokenId, next) {
		LogModule.log(TAG, 'login');
		var self = this;
		if(tokenId == null) {
			next(false);
		} else {
			var requestUrl = self.GOOGLE_AUTH_URL + tokenId;
			request(requestUrl, function(err, res, body) {
				if(err == null) {
					var data = UtilityModule.parseJson(body);
					LogModule.log(TAG, 'Google Auth Data', data);
					if(data != null && data[self.GOOGLE_AUTH_API.CLIENT_ID] == self.CLIENT_ID) {
						var googleId = data[self.GOOGLE_AUTH_API.GOOGLE_ID];
						var name = data[self.GOOGLE_AUTH_API.NAME];
						var email = data[self.GOOGLE_AUTH_API.EMAIL];
						var picture = data[self.GOOGLE_AUTH_API.PROFILE_PIC_URL];
						next(true, googleId, name, email, picture);
					} else {
						next(false);
					}
				} else {
					LogModule.log(TAG, err);
					next(false);
				}
			});
		}
	}

};

LogModule.logConstructor(TAG);

exports.GoogleLoginService = GoogleLoginService;