var UserModel = function() {

	var self = this;
	self.TAG = 'UserModel';
	LogModule.logConstructor(self.TAG);
	self.userId = null;
	self.sessionId = null;
	self.googleId = null;
	self.name = null;
	self.email = null;
	self.picture = null;

	self.clear = function() {
		LogModule.log(self.TAG, 'clear');
		self.userId = null;
		self.sessionId = null;
		self.googleId = null;
		self.name = null;
		self.email = null;
		self.picture = null;
	};

	self.login = function(userId, sessionId, googleId, name, email, picture) {
		LogModule.log(self.TAG, 'login');
		self.userId = userId;
		self.sessionId = sessionId;
		self.googleId = googleId;
		self.name = name;
		self.email = email;
		self.picture = picture;
		self.write();
	};

	self.logout = function(userId, sessionId, googleId, name, email, picture) {
		LogModule.log(self.TAG, 'logout');
		self.clear();
		self.write();
	};

	self.isLoggedIn = function() {
		LogModule.log(self.TAG, 'isLoggedIn');
		return !_.isNull(self.sessionId) && !_.isUndefined(self.sessionId) && self.sessionId != '';
	};

	self.write = function() {
		LogModule.log(self.TAG, 'write');
		if(self.userId == null) {
			StorageModule.remove(StorageModule.KEY.USER_ID);
		} else {
			StorageModule.set(StorageModule.KEY.USER_ID, self.userId);
		}
		if(self.sessionId == null) {
			StorageModule.remove(StorageModule.KEY.USER_SESSION_ID);
		} else {
			StorageModule.set(StorageModule.KEY.USER_SESSION_ID, self.sessionId);
		}
		if(self.googleId == null) {
			StorageModule.remove(StorageModule.KEY.USER_GOOGLE_ID);
		} else {
			StorageModule.set(StorageModule.KEY.USER_GOOGLE_ID, self.googleId);
		}
		if(self.name == null) {
			StorageModule.remove(StorageModule.KEY.USER_NAME);
		} else {
			StorageModule.set(StorageModule.KEY.USER_NAME, self.name);
		}
		if(self.email == null) {
			StorageModule.remove(StorageModule.KEY.USER_EMAIL);
		} else {
			StorageModule.set(StorageModule.KEY.USER_EMAIL, self.email);
		}
		if(self.picture == null) {
			StorageModule.remove(StorageModule.KEY.USER_PICTURE);
		} else {
			StorageModule.set(StorageModule.KEY.USER_PICTURE, self.picture);
		}
	};

	self.read = function() {
		LogModule.log(self.TAG, 'read');
		self.userId = StorageModule.get(StorageModule.KEY.USER_ID);
		self.sessionId = StorageModule.get(StorageModule.KEY.USER_SESSION_ID);
		self.googleId = StorageModule.get(StorageModule.KEY.USER_GOOGLE_ID);
		self.name = StorageModule.get(StorageModule.KEY.USER_NAME);
		self.email = StorageModule.get(StorageModule.KEY.USER_EMAIL);
		self.picture = StorageModule.get(StorageModule.KEY.USER_PICTURE);
	};

};

var USER = new UserModel();
USER.read();