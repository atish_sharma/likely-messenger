var GoogleLoginService = {

	TAG : 'GoogleLoginService',
	GOOGLE_CLIENT_ID : '131367081970-khbu0qjb3hl0i5h8flkhpl0ue67g30m4.apps.googleusercontent.com',

	init : function() {
		LogModule.log(this.TAG, 'init');
		var self = this;
		gapi.load('auth2', function() {
        auth2 = gapi.auth2.init({
          client_id: self.GOOGLE_CLIENT_ID,
        });
      });
	}

};

LogModule.logConstructor(GoogleLoginService.TAG);