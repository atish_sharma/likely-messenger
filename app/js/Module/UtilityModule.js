var UtilityModule = {

	TAG : 'UtilityModule',

	toast : function(str) {
		LogModule.log(this.TAG, 'toast');
		Materialize.toast(str, 3000);
	}

};

LogModule.logConstructor(UtilityModule.TAG);