var ApiModule = {

	TAG : 'ApiModule',

	KEY : {
		GOOGLE_AUTH_TOKEN_ID : 'google_auth_token_id',
		NAME : 'name',
		EMAIL : 'email',
		GOOGLE_ID : 'google_id',
		PROFILE_PIC_URL : 'profile_pic_url',
		SESSION_ID : 'session_id',
		ERROR : 'error',
		SUCCESS : 'success',
		USER_ID : 'user_id',
		SIGNUP_TIMESTAMP : 'signup_timestamp',
		SESSION_TIMESTAMP : 'session_timestamp',
		ACTIVE_SESSIONS : 'active_sessions',
		LIMIT : 'limit',
		USERS : 'users',
		MESSAGE : 'message',
		MESSAGES : 'messages',
		MESSAGE_TIMESTAMP : 'message_timestamp',
		MESSAGE_ID : 'message_id',
		RECEIVER_ID : 'receiver_id',
		SENDER_ID : 'sender_id',
		NOTIFIED : 'notified',
		TAGS : 'tags',
		SCORE : 'score',
		NOTIFICATIONS : 'notifications',
		MESSAGE_COUNT : 'message_count',
		LATEST_MESSAGE_TIMESTAMP : 'latest_message_timestamp',
		GET_MESSAGE_TYPE : 'get_message_type',
		CHAT_TIMESTAMP : 'chat_timestamp',
		SEEN : 'seen'
	},

	ERROR : {
		GOOGLE_LOGIN_FAILED : 0,
		INVALID_SESSION : 1,
		LOGOUT_FAILED : 2,
		SESSION_CLEARANCE_FAILED : 3,
		USER_SEARCH_FAILED : 4,
		MESSAGE_FAILED : 5,
		NOTIFICATION_FAILED : 6,
		CHAT_SEEN_FAILED : 7,
		CHAT_FAILED : 8
	}

};

LogModule.logConstructor(ApiModule.TAG);