var LogModule = {

	TAG : 'LogModule',
	DEBUG : true,
	INVOCATION : '<<invocation>>',
	CONSTRUCTOR : '<<constructor>>',

	log : function(tag, data, json) {
		if(this.DEBUG) {
			console.log(tag, data);
			if(!_.isUndefined(json) && !_.isNull(json)) {
				console.log('........................................');
				// console.log(JSON.stringify(json, null, 2));
				console.log(json);
			}
			console.log('----------------------------------------');
		}
	},

	logInvocation : function(tag) {
		this.log(tag, this.INVOCATION);
	},

	logConstructor : function(tag) {
		this.log(tag, this.CONSTRUCTOR);
	}

};

LogModule.logConstructor(LogModule.TAG);