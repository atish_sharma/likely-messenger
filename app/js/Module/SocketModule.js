var SocketModule = {

	TAG : 'SocketModule',
	SOCKET_EVENT : {
		CONNECT : 'connect',
		DISCONNECT : 'disconnect',
		GOOGLE_LOGIN : 'google_login',
		SESSION_LOGIN : 'session_login',
		LOGOUT : 'logout',
		SESSION_CLEARANCE : 'clear_sessions',
		USER_SEARCH : 'user_search',
		MESSAGE : 'message',
		MESSAGE_STATUS : 'message_status',
		SUGGESTIONS : 'suggestions',
		NOTIFICATION : 'notification',
		CHAT_SEEN : 'chat_seen',
		CHAT : 'chat'
	},
	LISTENER_ID : 'SocketModuleListener',

	socket : null,
	listeners : {},

	init : function() {
		LogModule.log(this.TAG, 'init');
		var self = this;
		self.socket = io();
		for(event in self.SOCKET_EVENT) {
			if(self.SOCKET_EVENT.hasOwnProperty(event)) {
				const VAL = self.SOCKET_EVENT[event];
				self.listeners[VAL] = {};
				self.socket.on(VAL, function(data) {
					LogModule.log(self.TAG, VAL, data);
					var listeners = self.listeners[VAL];
					for(var listener in listeners) {
						if(listeners.hasOwnProperty(listener)) {
							try {
								listeners[listener](data);
							} catch(err) {
								LogModule.log(self.TAG, err);
							}
						}
					}
				});
			}
		}
	},

	addListener : function(event, listenerId, listener) {
		LogModule.log(this.TAG, 'addListener');
		this.listeners[event][listenerId] = listener;
	},

	removeListener : function(event, listenerId) {
		LogModule.log(this.TAG, 'removeListener');
		try {
			delete this.listeners[event].listenerId;
		} catch(err) {
			LogModule.log(this.TAG, err);
		}	
	},

	emit : function(event, data) {
		LogModule.log(this.TAG, 'emit');
		LogModule.log(this.TAG, event, data);
		this.socket.emit(event, data);
	} 

};

LogModule.logConstructor(SocketModule.TAG);

SocketModule.init();

SocketModule.addListener(SocketModule.SOCKET_EVENT.CONNECT, SocketModule.LISTENER_ID, function() {
	UtilityModule.toast('Connection established.');
});

SocketModule.addListener(SocketModule.SOCKET_EVENT.DISCONNECT, SocketModule.LISTENER_ID, function() {
	UtilityModule.toast('Connection broken! Please check your internet connection.');
});