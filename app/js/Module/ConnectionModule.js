var ConnectionModule = {

	TAG : 'ConnectionModule',
	LISTENER_ID : 'ConnectionModuleListener',

	init : function() {
		LogModule.log(this.TAG, 'init');
		SocketModule.addListener(SocketModule.SOCKET_EVENT.CONNECT, this.LISTENER_ID, ConnectionModule.reconnect);
		SocketModule.addListener(SocketModule.SOCKET_EVENT.SESSION_LOGIN, this.LISTENER_ID, ConnectionModule.sessionLogin);
	},

	reconnect : function() {
		LogModule.log(ConnectionModule.TAG, 'reconnect');
		if(USER.isLoggedIn()) {
			var data = {};
			data[ApiModule.KEY.SESSION_ID] = USER.sessionId;
			SocketModule.emit(SocketModule.SOCKET_EVENT.SESSION_LOGIN, data);
		}
	},

	sessionLogin : function(data) {
		LogModule.log(ConnectionModule.TAG, 'sessionLogin');
		if(data[ApiModule.KEY.SUCCESS]) {
			USER.login(data[ApiModule.KEY.USER_ID], data[ApiModule.KEY.SESSION_ID],
				data[ApiModule.KEY.GOOGLE_ID], data[ApiModule.KEY.NAME], 
				data[ApiModule.KEY.EMAIL], data[ApiModule.KEY.PROFILE_PIC_URL]);
		} else {
			UtilityModule.toast('Invalid session! Please logout and login again.');
		}
	}

};

LogModule.logConstructor(ConnectionModule.TAG);

ConnectionModule.init();