var StorageModule = {

	TAG : 'StorageModule',
	KEY : {
		USER_NAME : 'use_name',
		USER_GOOGLE_ID : 'user_google_id',
		USER_SESSION_ID : 'user_session_id',
		USER_ID : 'user_id',
		USER_EMAIL : 'user_email',
		USER_PICTURE : 'user_picture'
	},

	set : function(key, val) {
		LogModule.log(this.TAG, 'set');
		localStorage.setItem(key, val);
	},

	get : function(key) {
		LogModule.log(this.TAG, 'get');
		return localStorage.getItem(key);
	},

	remove : function(key) {
		LogModule.log(this.TAG, 'remove');
		localStorage.removeItem(key);
	}

};

LogModule.logConstructor(StorageModule.TAG);