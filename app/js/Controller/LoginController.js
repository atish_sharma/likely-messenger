app.controller('LoginController', function($scope,$route, $routeParams,$location) {

	$scope.TAG = 'LoginController';
	$scope.LISTENER_ID = 'LoginControllerListener';
	$scope.user = USER;

	$scope.init = function() {
		LogModule.log($scope.TAG, 'init');
		SocketModule.addListener(SocketModule.SOCKET_EVENT.GOOGLE_LOGIN, $scope.LISTENER_ID, $scope.googleLoginResponse);
		if($scope.user.isLoggedIn()) {
			UtilityModule.toast('Welcome back ' + $scope.user.name);
			$scope.switchView('/messenger');
		}
	};

	$scope.fin = function() {
		LogModule.log($scope.TAG, 'fin');
		SocketModule.removeListener(SocketModule.SOCKET_EVENT.GOOGLE_LOGIN, $scope.LISTENER_ID);
	};

	$scope.$on('$routeChangeStart', function(next,current) {
       	LogModule.log($scope.TAG, '$routeChangeStart');
       	$scope.fin();
	});

	$scope.$on('$routeChangeSuccess', function(next,current) {
       	LogModule.log($scope.TAG, '$routeChangeSuccess');
		$scope.init();
	});

	$scope.googleLogin = function() {	
		LogModule.log($scope.TAG, 'googleLogin');
		/*
		auth2.grantOfflineAccess({'redirect_uri': 'postmessage'}).then(function(data) {
			LogModule.log($scope.TAG, 'data', data);
			var res = {};		
			res[ApiModule.KEY.GOOGLE_AUTH_TOKEN_ID] = data['Zi']['id_token'];
			SocketModule.emit(SocketModule.SOCKET_EVENT.GOOGLE_LOGIN, res);
		});
		*/
		auth2.signIn().then(function(data) {
			LogModule.log($scope.TAG, 'data', data);
			var res = {};		
			res[ApiModule.KEY.GOOGLE_AUTH_TOKEN_ID] = data['Zi']['id_token'];
			SocketModule.emit(SocketModule.SOCKET_EVENT.GOOGLE_LOGIN, res);
		});
	};

	$scope.googleLoginResponse = function(data) {       	
		LogModule.log($scope.TAG, 'googleLoginResponse');
		if(data[ApiModule.KEY.SUCCESS]) {
			$scope.user.login(data[ApiModule.KEY.USER_ID], data[ApiModule.KEY.SESSION_ID],
				data[ApiModule.KEY.GOOGLE_ID], data[ApiModule.KEY.NAME], 
				data[ApiModule.KEY.EMAIL], data[ApiModule.KEY.PROFILE_PIC_URL]);
			UtilityModule.toast('Welcome ' + $scope.user.name);
			$scope.switchView('/messenger'); 
		} else {
			UtilityModule.toast('Unable to login! Please try again.');
		}
	};

	$scope.switchView = function(view) {	
		LogModule.log($scope.TAG, 'switchView');
		$location.path(view);
		$scope.$apply();
	};

});