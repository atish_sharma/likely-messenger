app.controller('MessageController', function($scope, $route, $routeParams, $location) {

	$scope.TAG = 'MessageController';
	$scope.LISTENER_ID = 'MessageControllerListener';
	$scope.user = USER;
	$scope.userSearchLimit = 3;
	$scope.chatName = '';
	$scope.chatReceiverId = '';
	$scope.showChatBox = false;
	$scope.chatMessages = [];
	$scope.suggestionUsers = [];
	$scope.suggestionTags = [];
	$scope.showSuggestions = false;
	$scope.notifs = [];
	$scope.chats = {};
	$scope.searchedUsers = [];

	$scope.init = function() {
		LogModule.log($scope.TAG, 'init');
		SocketModule.addListener(SocketModule.SOCKET_EVENT.LOGOUT, $scope.LISTENER_ID, $scope.logoutResponse);
		SocketModule.addListener(SocketModule.SOCKET_EVENT.MESSAGE, $scope.LISTENER_ID, $scope.messageResponse);
		SocketModule.addListener(SocketModule.SOCKET_EVENT.MESSAGE_STATUS, $scope.LISTENER_ID, $scope.messageStatus);
		SocketModule.addListener(SocketModule.SOCKET_EVENT.NOTIFICATION, $scope.LISTENER_ID, $scope.notifications);
		SocketModule.addListener(SocketModule.SOCKET_EVENT.SUGGESTIONS, $scope.LISTENER_ID, $scope.suggestions);
		SocketModule.addListener(SocketModule.SOCKET_EVENT.USER_SEARCH, $scope.LISTENER_ID, $scope.userSearchResponse);
		// SocketModule.addListener(SocketModule.SOCKET_EVENT.SESSION_LOGIN, $scope.LISTENER_ID, $scope.sessionLoginResponse);
		if(!$scope.user.isLoggedIn()) {
			$scope.switchView('/login');
		} else {
			$scope.requestNotifications();
		}
	};

	$scope.fin = function() {
		LogModule.log($scope.TAG, 'fin');
		SocketModule.removeListener(SocketModule.SOCKET_EVENT.LOGOUT, $scope.LISTENER_ID);
		SocketModule.removeListener(SocketModule.SOCKET_EVENT.MESSAGE, $scope.LISTENER_ID);
		SocketModule.removeListener(SocketModule.SOCKET_EVENT.MESSAGE_STATUS, $scope.LISTENER_ID);
		SocketModule.removeListener(SocketModule.SOCKET_EVENT.NOTIFICATION, $scope.LISTENER_ID);
		SocketModule.removeListener(SocketModule.SOCKET_EVENT.SUGGESTIONS, $scope.LISTENER_ID);
		SocketModule.removeListener(SocketModule.SOCKET_EVENT.USER_SEARCH, $scope.LISTENER_ID);
		// SocketModule.removeListener(SocketModule.SOCKET_EVENT.SESSION_LOGIN, $scope.LISTENER_ID);
		$scope.chatName = '';
		$scope.chatReceiverId = '';
		$scope.showChatBox = false;
		$scope.chatMessages = [];
		$scope.suggestionUsers = [];
		$scope.suggestionTags = [];
		$scope.showSuggestions = false;
		$scope.notifs = [];
		$scope.chats = {};
		$scope.searchedUsers = [];
	};

	$scope.$on('$routeChangeStart', function(next,current) {
       	LogModule.log($scope.TAG, '$routeChangeStart');
       	$scope.fin();
	});

	$scope.$on('$routeChangeSuccess', function(next,current) {
       	LogModule.log($scope.TAG, '$routeChangeSuccess');
		$scope.init();
	});

	$scope.setChat = function(receiverId, name) {
		$scope.chatReceiverId = receiverId;
		$scope.chatName = name;
		$scope.showChatBox = true;
		$scope.chatMessages = [];
	};

	$scope.logout = function() {
       	LogModule.log($scope.TAG, 'logout');
       	var data = {};
		data[ApiModule.KEY.SESSION_ID] = $scope.user.sessionId;
		SocketModule.emit(SocketModule.SOCKET_EVENT.LOGOUT, data);
	};

	$scope.logoutResponse = function(data) {
       	LogModule.log($scope.TAG, 'logoutResponse');
       	if(data[ApiModule.KEY.SUCCESS] || data[ApiModule.KEY.ERROR] == ApiModule.ERROR.INVALID_SESSION) {
			$scope.user.logout();
			UtilityModule.toast('Logout successful.');
			$scope.switchView('/login');
		} else {
			UtilityModule.toast('Unable to log you out! Please try refreshing the page.');
		}
	};

	$scope.message = function(msg) {
       	LogModule.log($scope.TAG, 'message');
       	var data = {};
       	data[ApiModule.KEY.SESSION_ID] = USER.sessionId;
       	data[ApiModule.KEY.RECEIVER_ID] = $scope.chatReceiverId;
       	data[ApiModule.KEY.MESSAGE] = msg;
       	$scope.msg = '';
       	SocketModule.emit(SocketModule.SOCKET_EVENT.MESSAGE, data);
	};

	$scope.messageResponse = function(data) {
       	LogModule.log($scope.TAG, 'messageResponse');
       	if(data[ApiModule.KEY.RECEIVER_ID] == $scope.chatReceiverId) {
       		data['align'] = 'message-right';
       		data['tag'] = 'YOU';
       		$scope.chatMessages.push(data);
       	} else if(data[ApiModule.KEY.RECEIVER_ID] == USER.userId) {
       		data['align'] = 'message-left';
       		data['tag'] = data[ApiModule.KEY.NAME];
       		if(data[ApiModule.KEY.SENDER_ID] == $scope.chatReceiverId) {
       			$scope.chatMessages.push(data);
       		} else {
       			self.requestNotifications();
       		}
       	}
       	$scope.$apply();
	};

	$scope.suggestions = function(data) {
       	LogModule.log($scope.TAG, 'suggestions');
       	$scope.suggestionTags = data[ApiModule.KEY.TAGS];
       	$scope.suggestionUsers = data[ApiModule.KEY.USERS];
	    $scope.showSuggestions = true;
       	if($scope.suggestionTags.length == 0 || $scope.suggestionUsers.length == 0) {
	       	$scope.showSuggestions = false;
       	} 
       	if($scope.suggestionUsers[0][ApiModule.KEY.SCORE] == 0) {
    		// $scope.showSuggestions = false;
       	}
       	LogModule.log($scope.TAG, $scope.suggestionUsers);
       	$scope.$apply();
	};

	$scope.messageStatus = function(data) {
       	LogModule.log($scope.TAG, 'messageStatus');
       	if(!data[ApiModule.KEY.SUCCESS]) {
       		if(data[ApiModule.KEY.ERROR] == ApiModule.ERROR.INVALID_SESSION) {
	       		UtilityModule.toast('Invalid session! Please logout and login again.');
       		} else {
	       		UtilityModule.toast('Failed to deliver message.');
       		}
       	}
	};

	$scope.requestNotifications = function(data) {
       	LogModule.log($scope.TAG, 'requestNotifications');
       	var data = {};
       	data[ApiModule.KEY.SESSION_ID] = USER.sessionId;
       	SocketModule.emit(SocketModule.SOCKET_EVENT.NOTIFICATION, data);
	};

	$scope.notifications = function(data) {
       	LogModule.log($scope.TAG, 'notifications');
       	if(data[ApiModule.KEY.SUCCESS]) {
       		$scope.notifs = data[ApiModule.KEY.NOTIFICATIONS];
       	} else {
       		$scope.notifs = [];
       		if(data[ApiModule.KEY.ERROR] == ApiModule.ERROR.INVALID_SESSION) {
	       		UtilityModule.toast('Invalid session! Please logout and login again.');
       		}
       	}
       	$scope.$apply();
	};

	$scope.userSearch = function(email) {
       	LogModule.log($scope.TAG, 'userSearch');
       	var data = {};
       	try {
       		email = email.trim();
	       	if(email == '') {
       			$scope.searchedUsers = [];
		       	// UtilityModule.toast('Please enter an email ID.');
	       		return;
	       	}
       	} catch(err) {
       		LogModule.log(TAG, err);
       		$scope.searchedUsers = [];
		    // UtilityModule.toast('Please enter an email ID.');
		    return;
       	}
       	data[ApiModule.KEY.SESSION_ID] = USER.sessionId;
       	data[ApiModule.KEY.EMAIL] = email;
       	data[ApiModule.KEY.LIMIT] = $scope.userSearchLimit;
       	SocketModule.emit(SocketModule.SOCKET_EVENT.USER_SEARCH, data);
	};

	$scope.userSearchResponse = function(data) {
       	LogModule.log($scope.TAG, 'userSearchResponse');
       	if(data[ApiModule.KEY.SUCCESS]) {
       		$scope.searchedUsers = data[ApiModule.KEY.USERS];
       	} else {
       		$scope.searchedUsers = [];
       		if(data[ApiModule.KEY.ERROR] == ApiModule.ERROR.INVALID_SESSION) {
	       		UtilityModule.toast('Invalid session! Please logout and login again.');
       		} else {
	       		UtilityModule.toast('Unable to search for user.');
       		}
       	}
       	$scope.$apply();
	};

	$scope.switchView = function(view) {	
		LogModule.log($scope.TAG, 'switchView');
		$location.path(view);
		$scope.$apply();
	};

	/*
	$scope.sessionLoginResponse = function(data) {
       	LogModule.log($scope.TAG, 'sessionLoginResponse');
       	if(data[ApiModule.KEY.SUCCESS]) {
			$scope.user.login(data[ApiModule.KEY.USER_ID], data[ApiModule.KEY.SESSION_ID],
				data[ApiModule.KEY.GOOGLE_ID], data[ApiModule.KEY.NAME], 
				data[ApiModule.KEY.EMAIL], data[ApiModule.KEY.PROFILE_PIC_URL]);
			UtilityModule.toast('Session updated.');
		} else {
			UtilityModule.toast('Invalid session! Please logout and login again.');
		}
	};

	$scope.sessionLogin = function() {
       	LogModule.log($scope.TAG, 'sessionLogin');
       	var data = {};
		data[ApiModule.KEY.SESSION_ID] = $scope.user.sessionId;
		SocketModule.emit(SocketModule.SOCKET_EVENT.SESSION_LOGIN, data);
	};
	*/

});