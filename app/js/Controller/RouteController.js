app.config(function($routeProvider) {
	$routeProvider
    .when('/', {
    	templateUrl : 'html/login.html',
        controller : 'LoginController'
    })
    .when('/login', {
    	templateUrl : 'html/login.html',
        controller : 'LoginController'
    })
    .when('/messenger', {
    	templateUrl : 'html/messenger.html',
        controller : 'MessageController'
    })
    .otherwise({
    	redirectTo:'/'
    });
});